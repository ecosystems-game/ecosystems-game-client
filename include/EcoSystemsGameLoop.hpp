#ifndef ECOSYSTEMS_ECOSYSTEMSGAMELOOP_HPP
#define ECOSYSTEMS_ECOSYSTEMSGAMELOOP_HPP

#include "data-objects/GraphicsObjects.hpp"

#include <eco-system/GameLoop.hpp>

#include <eco-graphics/RenderWindow.hpp>
#include <eco-graphics/Device.hpp>
#include <eco-graphics/Renderer.hpp>
#include <eco-graphics/RenderSystem.hpp>

#include <GLFW/glfw3.h>

namespace ecosystems::client {
    class EcoSystemsGameLoop : public system::GameLoop {
    public:
        EcoSystemsGameLoop() : system::GameLoop() {
                // We set the window pointer to the GameLoop as it will be used for user input
                //glfwSetWindowUserPointer(m_render_window.get_glfw_window_ptr(), this);

                // Bind Keyboard Callbacks
//                glfwSetKeyCallback(m_render_window.get_glfw_window_ptr(), keypress_callback);
//                glfwSetCharCallback(m_render_window.get_glfw_window_ptr(), character_callback);
//                glfwSetCharModsCallback(m_render_window.get_glfw_window_ptr(), character_mods_callback);
//                glfwSetMouseButtonCallback(m_render_window.get_glfw_window_ptr(), mouse_button_callback);
//                glfwSetCursorPosCallback(m_render_window.get_glfw_window_ptr(), cursor_position_callback);
        };

        ~EcoSystemsGameLoop() {
            unload_scene();
        }

        data_objects::GraphicsObjects& get_graphics_objects() { return m_graphics_objects; }
        void tick(float p_delta_time) override;
    private:
        data_objects::GraphicsObjects m_graphics_objects{};

        // GLFW Input Callbacks
        static void keypress_callback(GLFWwindow *p_window, int p_key, int p_scancode, int p_action, int p_mods);
        static void cursor_position_callback(GLFWwindow *p_window, double p_xpos, double p_ypos);
        static void mouse_button_callback(GLFWwindow *p_window, int p_button, int p_action, int p_mods);
        static void character_mods_callback(GLFWwindow *p_window, unsigned int p_code_point, int p_mods);
        static void character_callback(GLFWwindow *p_window, unsigned int p_code_point);
    };
}

#endif //ECOSYSTEMS_ECOSYSTEMSGAMELOOP_HPP
