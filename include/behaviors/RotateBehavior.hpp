#ifndef ECOSYSTEMS_ROTATEBEHAVIOR_HPP
#define ECOSYSTEMS_ROTATEBEHAVIOR_HPP

#include <eco-system/Behavior.hpp>
#include <eco-system/components/TransformComponent.hpp>


namespace ecosystems::client::behaviors {
class RotateBehavior: public system::Behavior {
    public:
        void onCreate(Behavior* p_behavior) {
            std::cout << "On Create Hit!" << std::endl;
        }
        void onUpdate(Behavior* p_behavior, float p_delta_time) {
            float speed = .5f;
            auto& transform = p_behavior->getComponent<system::components::TransformComponent>();
            transform.rotation += glm::vec3(speed*p_delta_time, speed*p_delta_time, speed*p_delta_time);
        }
        void onDestroy(Behavior*) {}
    };
}

#endif //ECOSYSTEMS_ROTATEBEHAVIOR_HPP
