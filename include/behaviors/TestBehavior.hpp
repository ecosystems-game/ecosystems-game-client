#ifndef ECOSYSTEMS_TESTSCRIPT_HPP
#define ECOSYSTEMS_TESTSCRIPT_HPP

#include "eco-system/Behavior.hpp"

namespace ecosystems::client::behaviors {
class TestBehavior: public system::Behavior {
    public:
        void onCreate(Behavior*) {
            std::cout << "On Create Hit!" << std::endl;
        }
        void onUpdate(Behavior*, float p_delta_time) {
            std::cout << "On Update Hit! " << p_delta_time << std::endl;
        }
        void onDestroy(Behavior*) {}
    };
}

#endif //ECOSYSTEMS_TESTSCRIPT_HPP
