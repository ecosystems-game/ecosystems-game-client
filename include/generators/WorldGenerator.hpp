#include <vector>
#include <memory>
#include <array>
#include <glm/glm.hpp>

#ifndef ECOSYSTEMS_CLIENT_WORLDGENERATIONSYSTEM_HPP
#define ECOSYSTEMS_CLIENT_WORLDGENERATIONSYSTEM_HPP

namespace ecosystems::generators {
    static const size_t chunk_size_x = 16;
    static const size_t chunk_size_y = 16;
    static const size_t chunk_size_z = 256;
    using chunk_t = std::array<std::array<std::array<uint32_t, chunk_size_z>, chunk_size_y>, chunk_size_x>;

    class WorldGenerator {
    public:
        WorldGenerator(uint32_t p_world_chunk_x=256, uint32_t p_world_chunk_y=256)
            : m_world_chunk_x(p_world_chunk_x), m_world_chunk_y(p_world_chunk_y)
        {}

        chunk_t* generateChunk(uint32_t p_chunk_x, uint32_t p_chunk_y) {
            chunk_t* chunk = new chunk_t;
            for(uint32_t x = 0; x < 16; ++x) {
                uint32_t world_x = p_chunk_x * chunk_size_x + x;
                for(uint32_t y = 0; y < 16; ++y) {
                    uint32_t world_y = p_chunk_y * chunk_size_y + y;
                    for(uint32_t z = 0; z < 256; ++z) {
                        (*chunk)[x][y][z] = generateBlock(world_x, world_y, z);
                    }
                }
            }

            return chunk;
        }

        uint32_t generateBlock(uint32_t p_x, uint32_t p_y, uint32_t p_z) {
            // TODO: Perlin noise, Make OverWritable
            if(p_z == 128) {
                return 1;
            } else {
                return 0;
            }
        }
    private:
        uint32_t m_world_chunk_x, m_world_chunk_y;
    };
}

#endif //ECOSYSTEMS_CLIENT_WORLDGENERATIONSYSTEM_HPP
