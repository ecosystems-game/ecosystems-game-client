#ifndef ECOSYSTEMS_VASE_HPP
#define ECOSYSTEMS_VASE_HPP

#include <eco-graphics/Model.hpp>
#include <iostream>


namespace ecosystems::client {
    class Vase {
    public:
        explicit Vase(graphics::Device& p_device);
    };
}


#endif //ECOSYSTEMS_VASE_HPP
