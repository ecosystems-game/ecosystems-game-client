#ifndef ECOSYSTEMS_GRAPHICSOBJECTS_HPP
#define ECOSYSTEMS_GRAPHICSOBJECTS_HPP

#include <eco-graphics/RenderWindow.hpp>
#include <eco-graphics/Renderer.hpp>
#include <eco-graphics/RenderSystem.hpp>

#include <memory>


namespace ecosystems::client::data_objects {
    struct GraphicsObjects {
        static constexpr int WIDTH = 800;
        static constexpr int HEIGHT = 600;

//        ~GraphicsObjects() {
//            renderer.~Renderer();
//            render_system.~RenderSystem();
//            render_window.~RenderWindow();
//            device.~Device();
//        }

        graphics::RenderWindow render_window{WIDTH, HEIGHT, "EcoSystems"};
        graphics::Device device{render_window};
        graphics::Renderer renderer{render_window, device};
        graphics::RenderSystem render_system {device, renderer.get_swap_chain_render_pass()};
    };
}

#endif //ECOSYSTEMS_GRAPHICSOBJECTS_HPP
