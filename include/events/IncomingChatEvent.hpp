#ifndef ECOSYSTEMS_INCOMINGCHATEVENT_HPP
#define ECOSYSTEMS_INCOMINGCHATEVENT_HPP

#include <random>
#include <string>

#include <boost/asio.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <eco-network/events/NetworkEvent.hpp>
#include <eco-network/Packet.hpp>
#include <eco-network/Session.hpp>


namespace ecosystems::client {
    class IncomingChatEvent : public network::events::NetworkEvent {
    public:
        IncomingChatEvent() : network::events::NetworkEvent() {}
        void Call(network::Packet& p_packet, std::shared_ptr<network::Session>& p_session, std::shared_ptr<network::events::NetworkObserver> p_observer) override {
            auto message = p_packet.ReadNextArgument<std::string>();
            auto second_string = p_packet.ReadNextArgument<std::string>();
            auto x = p_packet.ReadNextArgument<int32_t>();
            auto y = p_packet.ReadNextArgument<float>();

            std::cout << "============ Message Received ============" << std::endl;
            if(p_packet.get_send_method() == network::UDP) {
                std::cout << "Message Protocol: [UDP]" << std::endl;
            } else {
                std::cout << "Message Protocol: [TCP]" << std::endl;
            }

            std::cout << "[Session Connection Test]" << std::endl;
            std::cout << "This that a session is fully connected through UDP, TCP, and with a proper Session ID" << std::endl;
            std::cout << "By This point, all listed prior should be set and the Session should be fully connected." << std::endl;
            std::cout << "If those are printed, this test worked without errors" << std::endl;

            boost::uuids::uuid SESSION_UNASSIGNED_UUID = boost::uuids::string_generator()("00000000-0000-0000-0000-000000000000");
            if(p_session->get_session_uuid() != SESSION_UNASSIGNED_UUID && p_session->is_tcp_connected() && p_session->is_udp_connected()) {
                std::cout << "It worked, Here are your results... :)" << std::endl << std::endl;
                std::string udp_endpoint = p_session->get_udp_endpoint().address().to_string() + ":" + std::to_string(p_session->get_udp_endpoint().port());
                std::string tcp_endpoint = p_session->get_tcp_socket().remote_endpoint().address().to_string() + ":" + std::to_string(p_session->get_tcp_socket().remote_endpoint().port());

                std::cout << "    UDP Endpoint: " << udp_endpoint << std::endl;
                std::cout << "    Session ID: " << p_session->get_session_uuid() << std::endl;
                std::cout << "    TCP Endpoint" << tcp_endpoint << std::endl;
            } else {
                std::cerr << "Well that's embarrassing, the Session isn't fully connected :c" << std::endl;
                std::cerr << "    session/is_tcp_connected: " << p_session->is_tcp_connected() << std::endl;
                std::cerr << "    session/is_udp_connected: " << p_session->is_udp_connected() << std::endl;
                std::cerr << "    session/get_session_uuid: " << p_session->get_session_uuid() << std::endl;
            }
            std::cout << std::endl << std::endl;

            std::cout << "Okay, Now I'm just gonna spam the server with garbage data..." << std::endl;

//            // Spam the server with garbage data
//            size_t packet_size = 1024;
//            std::random_device rnd;
//            size_t iterations = 128;
//            for(size_t it  = 0; it < iterations; ++it){
//                std::vector<char> m_buffer(packet_size);
//                for(size_t i = 0; i < packet_size; ++i)
//                {
//                    m_buffer.push_back(rnd());
//                }
//                network::Packet spam(&m_buffer[0], packet_size, network::TCP);
//                spam.EchoReadBuffer();
//                p_session->UDP_Send(spam);
//                p_session->TCP_Send(spam);
//            }
//            std::cout << std::endl;
            std::cout << "Now attempting to send out real data..." << std::endl;

            ecosystems::network::Packet p(p_observer->get_hook_index("player-chat"));
            p.AddArgument("Hello Network!");
            //std::string ipsum("Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet consectetur adipisci[ng] velit, sed quia non numquam [do] eius modi tempora inci[di]dunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum[d] exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? [D]Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? [33] At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.");
            //p.AddArgument(ipsum);
            p.AddArgument("This is a response from the client!");
            p_session->UDP_Send(p);
            p_session->TCP_Send(p);
//            std::cout << "String Arg1: "<< message << std::endl;
//            std::cout << "String Arg2: " << second_string << std::endl;
//            std::cout << "Integer Arg3: " << x << std::endl;
//            std::cout << "Float Arg4: " << y << std::endl;
        }
    };
}

#endif //ECOSYSTEMS_INCOMINGCHATEVENT_HPP
