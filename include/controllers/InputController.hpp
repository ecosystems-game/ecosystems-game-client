#ifndef ECOSYSTEMS_INPUTCONTROLLER_HPP
#define ECOSYSTEMS_INPUTCONTROLLER_HPP

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>
#include <iostream>

namespace ecosystems::system {
    class InputController {
    public:
        virtual void onKeyDown(int p_key, int p_scancode, int p_mods) {};
        virtual void onKeyUp(int p_key, int p_scancode, int p_mods) {};
        virtual void onCursorMove(double p_xpos, double p_ypos, glm::fvec2 p_mouse_position) {};
        virtual void onMouseButtonUp(int p_button, int p_mods, glm::fvec2 p_mouse_position) {};
        virtual void onMouseButtonDown(int p_button, int p_mods, glm::fvec2 p_mouse_position) {};
    };
}
#endif //ECOSYSTEMS_INPUTCONTROLLER_HPP
