#include "scenes/MainGameScene.hpp"
#include "behaviors/TestBehavior.hpp"
#include "behaviors/RotateBehavior.hpp"
#include "data-objects/GraphicsObjects.hpp"

#include <eco-system/components/BehaviorComponent.hpp>
#include <eco-system/components/TransformComponent.hpp>

#include <eco-graphics/RenderWindow.hpp>
#include <eco-graphics/Device.hpp>
#include <eco-graphics/Renderer.hpp>
#include <eco-graphics-ccl/components/CameraComponent.hpp>
#include <eco-graphics-ccl/components/MeshRenderComponent.hpp>

#include <GLFW/glfw3.h>


namespace ecosystems::client::views {

    client::views::MainGameScene::MainGameScene(data_objects::GraphicsObjects& p_graphics_objects)
        : system::Scene(), m_graphics_objects(p_graphics_objects) {}

    void MainGameScene::mounted() {
        loadGameObjects();
    }

    void MainGameScene::loadGameObjects() {
        m_camera = createEntity("camera");
        //entity.addComponent<system::components::BehaviorComponent>().bind<behaviors::TestBehavior>();
        auto& cameraComponent = m_camera.addComponent<ccl::graphics::components::CameraComponent>();
        auto& cameraTransform = m_camera.getComponent<system::components::TransformComponent>();
        cameraTransform.translation = {0.f, 0.f, 0.f};

        m_model = createEntity("model");
        const char* modelFile = "data/models/colored_cube.obj";
        m_model.addComponent<ccl::graphics::components::MeshRenderComponent>(
                graphics::Model::createModelFromFile(m_graphics_objects.device, modelFile), &m_graphics_objects.render_system);
        auto& modelTransform = m_model.getComponent<system::components::TransformComponent>();
        modelTransform.translation = {0.f, 0.f, -2.5f};
        modelTransform.scale = .5f;
        m_model.addComponent<system::components::BehaviorComponent>().bind<behaviors::RotateBehavior>();
    }

    void MainGameScene::update(float p_delta_time) {
        // Run the system events
        system::Scene::update(p_delta_time);

        auto& cameraComponent = m_camera.getComponent<ecosystems::ccl::graphics::components::CameraComponent>();
        auto& cameraTransform = m_camera.getComponent<system::components::TransformComponent>();
        auto projectionView = cameraComponent.projection_matrix * cameraComponent.view_matrix;
        auto& modelTransform = m_model.getComponent<system::components::TransformComponent>();
        auto modelMatrix = modelTransform.mat4();

        if(auto commandBuffer = m_graphics_objects.renderer.BeginFrame()) {
            m_graphics_objects.renderer.BeginSwapChainRenderPass(commandBuffer);
            m_graphics_objects.render_system.bindCommandBuffer(commandBuffer);

            m_registry.view<ecosystems::ccl::graphics::components::MeshRenderComponent>().each([=](auto p_entity, auto& p_mrc) {
                p_mrc.draw(commandBuffer, modelMatrix, projectionView);
            });

            m_graphics_objects.renderer.EndSwapChainRenderPass(commandBuffer);
            m_graphics_objects.renderer.EndFrame();
        }

        cameraComponent.setViewTarget(cameraTransform.translation, modelTransform.translation);
        cameraComponent.setPerspectiveProjection(glm::radians(50.f), m_graphics_objects.renderer.get_aspect_ratio(), 0.1, 10.f);
    }
}