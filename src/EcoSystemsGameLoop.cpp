#include "EcoSystemsGameLoop.hpp"
#include <eco-graphics/RenderSystem.hpp>


namespace ecosystems::client {

    // We override the run function because we are using the GLFW Render Windows and need to include a pollEvents call
    void EcoSystemsGameLoop::tick(float p_delta_time) {
        // End the gameloop if the window closes
        if(!m_graphics_objects.render_window.is_open()) {
            m_should_exit = true;
            return;
        }

        glfwPollEvents();

        // Run the Scene's Update Function
        system::GameLoop::tick(p_delta_time);
    }

//    void EcoSystemsGameLoop::draw() {
//        if(auto commandBuffer = m_renderer.BeginFrame()) {
//            m_renderer.BeginSwapChainRenderPass(commandBuffer);
//            m_simple_render_system.RenderGameObjects(commandBuffer, m_game_view->get_game_objects(), m_game_view->get_camera());
//            m_renderer.EndSwapChainRenderPass(commandBuffer);
//            m_renderer.EndFrame();
//        }
//    }

    void EcoSystemsGameLoop::keypress_callback(GLFWwindow *p_window, int p_key, int p_scancode, int p_action, int p_mods) {
        auto gameLoop = reinterpret_cast<GameLoop*>(glfwGetWindowUserPointer(p_window));

        if(p_action == GLFW_PRESS || p_action == GLFW_REPEAT) {
            //gameLoop->get_game_view()->get_input_controller()->onKeyDown(p_key, p_scancode, p_mods);
        } else if (p_action == GLFW_RELEASE) {
            //gameLoop->get_game_view()->get_input_controller()->onKeyUp(p_key, p_scancode, p_mods);
        }
    }

    void EcoSystemsGameLoop::cursor_position_callback(GLFWwindow *p_window, double p_xpos, double p_ypos) {
        auto gameLoop = reinterpret_cast<GameLoop*>(glfwGetWindowUserPointer(p_window));

        glm::dvec2  mousePosition(p_xpos, p_ypos);
        glfwGetCursorPos(p_window, &mousePosition.x, &mousePosition.y);
        //mousePosition.y = -(mousePosition.y - EcoSystemsGameLoop::HEIGHT);

        //gameLoop->get_game_view()->get_input_controller()->onCursorMove(p_xpos, p_ypos, mousePosition);
    }

    void EcoSystemsGameLoop::mouse_button_callback(GLFWwindow *p_window, int p_button, int p_action, int p_mods) {
        auto gameLoop = reinterpret_cast<EcoSystemsGameLoop*>(glfwGetWindowUserPointer(p_window));

        glm::dvec2  mousePosition(0, 0);
        glfwGetCursorPos(p_window, &mousePosition.x, &mousePosition.y);
        //mousePosition.y = -(mousePosition.y - GameLoop::HEIGHT);

        if (p_action == GLFW_PRESS || p_action == GLFW_REPEAT) {
            //EcoSystemsGameLoop->get_game_view()->get_input_controller()->onMouseButtonDown(p_button, p_mods, mousePosition);

        } else if (p_action == GLFW_RELEASE) {
            //EcoSystemsGameLoop->get_game_view()->get_input_controller()->onMouseButtonUp(p_button, p_mods, mousePosition);
        }
    }

    void EcoSystemsGameLoop::character_mods_callback(GLFWwindow *p_window, unsigned int p_code_point, int p_mods) {
        auto gameLoop = reinterpret_cast<EcoSystemsGameLoop *>(glfwGetWindowUserPointer(p_window));
    }

    void EcoSystemsGameLoop::character_callback(GLFWwindow *p_window, unsigned int p_code_point) {
        EcoSystemsGameLoop::character_mods_callback(p_window, p_code_point, 0);
    }
}