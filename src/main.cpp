#include "EcoSystemsGameLoop.hpp"
#include "scenes/MainGameScene.hpp"
#include "events/IncomingChatEvent.hpp"

#include <eco-system/GameLoop.hpp>
#include <eco-network/Packet.hpp>
#include <eco-network/Client.hpp>

#include <iostream>
#include <cstdlib>
#include <vector>
#include <memory>


int main() {
//    auto client = std::make_shared<ecosystems::network::Client>();
//    client->get_observer()->SubscribeEvent("player-chat", new ecosystems::client::IncomingChatEvent());
//    ecosystems::network::Packet packet(client->get_observer()->get_hook_index("player-chat"));
//    client->Connect("127.0.0.1", 3000);
//    while(!client->get_session()->is_connected()) {}
//    client->TCP_Send(packet);
//    client->UDP_Send(packet);

    ecosystems::client::EcoSystemsGameLoop gameLoop;
    gameLoop.load_scene(std::make_unique<ecosystems::client::views::MainGameScene>(gameLoop.get_graphics_objects()));

    try {
        return gameLoop.run();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}