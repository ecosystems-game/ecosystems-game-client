//#include "include/game-objects/Block.hpp"
//#include "include/behaviors/RotateBehavior.hpp"
//
//
//namespace ecosystems::client {
//    Block::Block(graphics::Device& p_device) : system::GameObject(system::GameObject::CreateGameObject()) {
//        model = CreateCubeModel(p_device);
//        m_texture = graphics::Texture::createTextureFromImageFile(p_device, "./data/textures/dirt.png");
//        bindBehavior(std::make_unique<behaviors::RotateBehavior>());
//    }
//
//    std::unique_ptr<graphics::Model> Block::CreateCubeModel(graphics::Device &p_device) {
//        graphics::Model::Builder builder{};
//        builder.vertices = {
//                {{-.5f, -.5f, -.5f}}, // 0: Left, Top, Front
//                {{-.5f, .5f, .5f}}, // 1: Left, Bottom, Back
//                {{-.5f, -.5f, .5f}}, // 2: Left, Top, Back
//                {{-.5f, .5f, -.5f}}, // 3: Left, Bottom, Front
//                {{.5f, -.5f, -.5f}}, // 4: Right, Top, Front
//                {{.5f, .5f, .5f}}, // 5: Right, Bottom, Back
//                {{.5f, -.5f, .5f}}, // 6: Right, Top, Back
//                {{.5f, .5f, -.5f}}, // 7: Right, Bottom, Front
//
//        };
//
//        builder.indices = {0, 1, 2,
//                           0, 3, 1,
//
//                           4, 5, 6,
//                           4, 7, 5,
//
//                           0, 4, 6,
//                           0, 6, 2,
//
//                           1, 3, 7,
//                           1, 7, 5,
//
//                           2, 6, 4,
//                           2, 4, 0,
//
//                           3, 7, 4,
//                           3, 4, 0};
//
//        return std::make_unique<graphics::Model>(p_device, builder);
//    }
//}