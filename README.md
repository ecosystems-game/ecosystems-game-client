# EcoSystems Game Client

EcoSystems Game Client is the official repository for the game client of EcoSystems. It implements various submodules from the EcoSystems game engine repository project. EcoSystems-System, Graphics, and Networking are all included as submodules under the /engines directory.

To clone the EcoSystem client project use `git clone --recurse-submodules https://gitlab.com/ecosystems-game/applications/ecosystems-client.git`

## Requirements
```
CMake version >= 3.16
Boost Version >= 1.71.0 (Asio, System, Filesystem)

```